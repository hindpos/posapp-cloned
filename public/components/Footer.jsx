import React from 'react';

var Footer = React.createClass({

	render: function () {
		return (
			<footer className="text-center">
				<h3>2017 Bharat Yantra Ltd.</h3>
			</footer>
		);
	}

});

module.exports = Footer;
